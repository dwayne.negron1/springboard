# Fake News, True Data
Fake news has become a present and constant factor in todays world of media coverage and/or content creation. Developing better strategies for combatting, identifying, and addressing false information is vital skill for any data scientist. It is also a great opportunity to refine my familiarity with NLP and develop a better fundamental understanding of ML algorithms.  

# 1. Data

Dataset was provided by Kaggle and was originally part of a study performed by University of Victoria. The data set has the following features and was divided into two sets (real and fake):

* Title
* Text
* Subject
* Date

Original Dataset can be found here: https://www.kaggle.com/c/fake-news/data
Original Dataset study can be found here: https://www.uvic.ca/engineering/ece/isot/assets/docs/ISOT_Fake_News_Dataset_ReadMe.pdf

# 2. Method

## Feature Engineering

SpaCy was used to help us extract features from the dataset. Primary features of observation were divided between what we could obtain without nlp() and what needed nlp(). Primary features looked at that did not need nlp() were the following for the title and text of entries: average length, amount of tokens within, average length of tokens. Utilizing NLP we able to extract the following features for title and text of entries: amount of stop words, percent of alpha characters present, the unique entities within and their count, the unique parts of speech and count. 

Our Features for this project were divided into four sets. 
* Set 1: 
    - title: [len(), token.count(), average_token.len(), stopwords.count(), % alpha characters]
    - text: [len(), token.count(), average_token.len(), stopwords.count(), % alpha characters]
* Set 2: Set 1 + Count of Individual Entities
* Set 3: Set 1 + Count of Individual Parts of Speech
* Set 4: Set 1 + Count of Individual Entities + Count of Individual Parts of Speech

For this project certain features native to the dataset were ignored to provide a better learning experience and avoid fallacies inherent in the dataset. Not much information was provided for how the **subject** column was generated for the false data but the disparity between its organization and the the real datas **subject** made it a strong [99%+] indicator of realness and so was discounted for the project to better focus on the potential NLP features. 

## Data Cleaning

In order to clean the data a few steps were taken. Some irregularities were noticed early on with the **subject** column of the dataset and it was not used in the experiment. 

Data was not missing any values pre or post feature engineering and was very manageable.   


# 3. EDA 

Our dataset for this project was balanced and had approximately 50,000 samples. While there was a good distribution of subjects we found that they were unbalanced with with two topics alone found in the real dataset and the other 6 in the false. 

# 4. Algorithms & Machine Learning

The primary algorithms utilized for this experiment were the following: 
* Logistic Regression
* Random Forest
* K-Nearest Neighbor
* Count Vectorizer / TfidfTransformer / MultinomialNB

In our ML model we found that Random Forest performed best on our classification problem. 

A main goal of this project was developing a better understanding of how ML algorithms would work and resolve different feature sets relevant to NLP. 

# 5. Observations

Our first set of models [Logistic Regression, Random Forest, KNN] quickly outperformed a basic pipeline of [CountVectorizer, TfidfTransformer, MultinomialNB] almost unamously in regards to our target variable. A reason for this could be the organization and development of the dataset. Unfortunately, we do not know the process in which the fake newset was developed. It is possible that the development of this dataset introduced patterns in the data which led to a simpler classification.

# 6. Conclusion

Determination of fake news can be an incredibly complicated problem which utilizes models like BERT. While complicated, the topic can be interacted with and combatted using relatively simple techniques and a bit of Python. It is my hope that with a better understanding on how fake news is generating and common practices to identify it we can become better situated for more permanent fake news classification.

If solutions can be reached with less resource intensive algorithms then hopefully anti-deepfake technologies can be more readily present and utilized in our day to day interactions with created content and news.  

# 7. Future Improvements

If I was to grow this project and perform a second iteration I think it would be interesting to utilize our model against an expanded Reuters newset and finetune the performance. If the model was able to perform strongly against an added dataset, I think the next step would be to readjust the model for a timeseries and try to predict the fake news per a time variable. 

Doing this project has inspired me to consider creating a text to speech translator to record live news and utilize some of our models against it see how much relates. 

